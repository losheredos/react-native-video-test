import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
import { Splash, VideoPlayer } from './screens'

const Router = createStackNavigator({
    Splash,
    VideoPlayer
},{
    defaultNavigationOptions:{
        headerShown: false,
    },
});


export default createAppContainer(Router);

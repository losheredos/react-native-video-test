const videos = [
    {title : "Big Buck Bunny", description : "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license", source : "https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4"},

    {title: 'Story', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut rhoncus lorem, a egestas nisi. Fusce a vestibulum leo. Pellentesque sodales hendrerit nisi, nec molestie odio ultrices sed. Nullam lacinia id purus eget fermentum. Vestibulum non volutpat enim, a cursus est. Sed nec interdum lorem. Maecenas at euismod est, non pretium ligula. Duis lacinia urna.', source: 'http://mirrors.standaloneinstaller.com/video-sample/P6090053.mp4'},

    {title: 'Canyon Magic', description: 'Magical Canyon Journey', source: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4 '},
    {title: 'A Toy', description: 'Meaningless toy', source: 'http://techslides.com/demos/sample-videos/small.mp4'},

    {title : "Big Buck Bunny", description : "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license", source : "https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4"},

    {title: 'Story', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut rhoncus lorem, a egestas nisi. Fusce a vestibulum leo. Pellentesque sodales hendrerit nisi, nec molestie odio ultrices sed. Nullam lacinia id purus eget fermentum. Vestibulum non volutpat enim, a cursus est. Sed nec interdum lorem. Maecenas at euismod est, non pretium ligula. Duis lacinia urna.', source: 'http://mirrors.standaloneinstaller.com/video-sample/P6090053.mp4'},

    {title: 'Canyon Magic', description: 'Magical Canyon Journey', source: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4 '},
    {title: 'A Toy', description: 'Meaningless toy', source: 'http://techslides.com/demos/sample-videos/small.mp4'},

    {title : "Big Buck Bunny", description : "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license", source : "https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4"},

    {title: 'Story', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut rhoncus lorem, a egestas nisi. Fusce a vestibulum leo. Pellentesque sodales hendrerit nisi, nec molestie odio ultrices sed. Nullam lacinia id purus eget fermentum. Vestibulum non volutpat enim, a cursus est. Sed nec interdum lorem. Maecenas at euismod est, non pretium ligula. Duis lacinia urna.', source: 'http://mirrors.standaloneinstaller.com/video-sample/P6090053.mp4'},

    {title: 'Canyon Magic', description: 'Magical Canyon Journey', source: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4 '},
    {title: 'A Toy', description: 'Meaningless toy', source: 'http://techslides.com/demos/sample-videos/small.mp4'},

    {title : "Big Buck Bunny", description : "Big Buck Bunny tells the story of a giant rabbit with a heart bigger than himself. When one sunny day three rodents rudely harass him, something snaps... and the rabbit ain't no bunny anymore! In the typical cartoon tradition he prepares the nasty rodents a comical revenge.\n\nLicensed under the Creative Commons Attribution license", source : "https://file-examples.com/wp-content/uploads/2017/04/file_example_MP4_480_1_5MG.mp4"},

    {title: 'Story', description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut rhoncus lorem, a egestas nisi. Fusce a vestibulum leo. Pellentesque sodales hendrerit nisi, nec molestie odio ultrices sed. Nullam lacinia id purus eget fermentum. Vestibulum non volutpat enim, a cursus est. Sed nec interdum lorem. Maecenas at euismod est, non pretium ligula. Duis lacinia urna.', source: 'http://mirrors.standaloneinstaller.com/video-sample/P6090053.mp4'},

    {title: 'Canyon Magic', description: 'Magical Canyon Journey', source: 'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4 '},
    {title: 'A Toy', description: 'Meaningless toy', source: 'http://techslides.com/demos/sample-videos/small.mp4'}

];

export default videos;

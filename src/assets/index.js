import videos from './videos';
import images from './images';

module.exports = {
    videos,
    images
};

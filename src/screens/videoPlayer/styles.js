import {StyleSheet} from 'react-native';
import {SH, SW, FULL_SH} from '../../utils';

const styles = StyleSheet.create({
    container:{
        flex: 1,
    },
    video:{
        width: '100%',
        height: FULL_SH*.76,
    },
    infoContainer:{
        marginTop: 5*SH,
        marginHorizontal: 10*SW
    },
    title:{
        fontSize: 16*SW
    },
    description:{
        fontSize: 14*SW
    },
    loadingIndicator:{
        position: 'absolute',
        top: FULL_SH*.38,
        left: '50%'
    }
});

export default styles;

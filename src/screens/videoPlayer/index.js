import React, {useState} from 'react';
import {SafeAreaView, View, Text, ActivityIndicator, ScrollView} from 'react-native';
import Video from 'react-native-video';
import styles from './styles';

const VideoPlayer = (props) => {
    const [loading, setLoading] = useState(true);
    const video = props.navigation.getParam('video');
    return(
        <SafeAreaView style={styles.container}>
            <Video
                source={{ uri: video.source }}
                style={styles.video}
                onLoad={() => setLoading(false)}
                onBuffer={() => console.warn('buffering')}
                resizeMode={'cover'}
                // controls={true}
                repeat
            />
            {loading && <ActivityIndicator style={styles.loadingIndicator} size={"large"} />}
            <ScrollView>
                <View style={styles.infoContainer}>
                    <Text style={styles.title}>{video.title}</Text>
                    <Text style={styles.description}>{video.description}</Text>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
};

export default VideoPlayer;

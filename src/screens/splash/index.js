import React, {Component} from 'react';
import {SafeAreaView, Text, View, Animated, FlatList} from 'react-native';
import {VideoListItem} from '../../components';
import {images, videos, FULL_SH, SH, actions, connect} from '../../utils';
import styles from './styles';

const MAX_HEADER_HEIGHT = FULL_SH/3;
const MIN_HEADER_HEIGHT = FULL_SH/6;
const SCROLL_DISTANCE = (MAX_HEADER_HEIGHT - MIN_HEADER_HEIGHT)*2;

class Splash extends Component{
    constructor() {
        super();
        this.state = {
            scrollY: new Animated.Value(0)
        }
    }

    _keyExtractor = (item, index) => index.toString();

    onVideoItemPressed(item){
        this.props.navigate('VideoPlayer', {video: item})
    }

    render(){
        const topContainerHeight = this.state.scrollY.interpolate({
            inputRange: [20, SCROLL_DISTANCE],
            outputRange: [MAX_HEADER_HEIGHT, MIN_HEADER_HEIGHT],
            extrapolate: 'clamp',
        });

        const colorInterpolation = this.state.scrollY.interpolate({
            inputRange: [20, SCROLL_DISTANCE/2, SCROLL_DISTANCE],
            outputRange: ['rgb(54,96,41)', 'rgb(75,130,55)', 'rgba(75,130,55,0.47)'],
            extrapolate: 'clamp',
        });

        const textColorInterpolation = this.state.scrollY.interpolate({
            inputRange: [100, SCROLL_DISTANCE],
            outputRange: ['rgb(255,249,250)', 'rgb(184,39,35)'],
            extrapolate: 'clamp',
        });

        return(
            <SafeAreaView style={styles.container}>
                <Animated.View
                    style={[styles.topContainer, {height: topContainerHeight, backgroundColor: colorInterpolation}]}
                    source={images.icons.youtube}
                >
                    <Animated.Text style={{color: textColorInterpolation}}>Something like header or Image</Animated.Text>
                </Animated.View>

                <FlatList
                    initialNumToRender={6}
                    style={styles.list}
                    data={videos}
                    keyExtractor={this._keyExtractor}
                    scrollEventThrottle={1}
                    ItemSeparatorComponent={() => <View style={styles.seperator} />}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.state.scrollY}}}]
                    )}
                    renderItem={({item, index}) => <VideoListItem item={item} onPress={item => this.onVideoItemPressed(item)} />}
                />
            </SafeAreaView>
        )
    }
}

export default connect(null, actions)(Splash);

import {StyleSheet} from 'react-native';
import {SH, SW} from '../../utils';

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems: 'center'
    },
    topContainer:{
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    list:{
        padding: 6*SW,
        marginBottom: 2*SH
    },
    seperator:{
        height: .5*SH,
        borderWidth: SW*.3,
        borderColor: 'gray',
        width: '100%'
    }
});

export default styles;

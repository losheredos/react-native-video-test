import Splash from './splash';
import VideoPlayer from './videoPlayer';

module.exports = {
    Splash,
    VideoPlayer
};

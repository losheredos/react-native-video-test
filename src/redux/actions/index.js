import {NAVIGATE} from './actionTypes';

export default function actions(dispatch) {
    return{
        // APP
        navigate: (screenName, params) => dispatch({type: NAVIGATE, screenName, params}),
    }
}

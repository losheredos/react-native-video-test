import {call, delay, put, takeLatest} from "@redux-saga/core/effects";
import {navigationService} from '../../utils';
import {NAVIGATE} from '../actions/actionTypes';

function* navigate(action) {
    const {screenName, params} = action;
    navigationService.navigate(screenName, params)
}

const appSaga = [
    takeLatest(NAVIGATE, navigate),
];

export default appSaga;

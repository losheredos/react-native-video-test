import React, {useState} from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import Video from 'react-native-video';
import {SH, SW, FULL_SW, PropTypes} from '../../utils';

const VideoListItem = (props) => {
    const [duration, setDuration] = useState(0);
    let {title, description, source} = props.item;
    const {onPress, item} = props;
    description = description.length > 45 ? description.substr(0, 45)+'...' : description;

    return(
        <TouchableOpacity onPress={() => onPress(item)} style={styles.container}>
            {/* Thumbnail için farklı paketler denedim ama işe yaramadılar */}
            <Video
                source={{ uri: source }}
                style={styles.thumbnail}
                onLoad={res => setDuration(res.duration)}
                onError={er => console.warn(er)}
                resizeMode={'cover'}
                paused
            />
            <View style={styles.infoContainer}>
                <Text style={styles.titleText}>{title}</Text>
                <Text style={styles.descriptionText}>{description}</Text>
            </View>
            <Text style={styles.durationText}>{duration === 0 ? '~' : duration.toFixed(1)+' sn'}</Text>
        </TouchableOpacity>
    )
};

VideoListItem.propTypes = {
    onPress: PropTypes.func,
    item: PropTypes.object,
};

export default VideoListItem;

const styles = StyleSheet.create({
   container:{
       width: FULL_SW*.92,
       alignSelf: 'center',
       height: 40*SH,
       flexDirection: 'row',
       justifyContent: 'space-between',
       alignItems: 'center',
       marginVertical: 2*SH
   },
    thumbnail:{
        width: FULL_SW*.27,
        height: '100%'
    },
    infoContainer:{
        width: FULL_SW*.33,
    },
    titleText:{
        fontSize: 14*SW
    },
    descriptionText:{
        fontSize: 11*SW
    },
    durationText:{
        width: FULL_SW*.15,
        fontSize: 12*SW,
        textAlign: 'center'
    }
});

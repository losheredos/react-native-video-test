import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import actions from '../redux/actions';
import {SW, SH, FULL_SW, FULL_SH, SCREEN_RATIO} from './dimensions';
import {videos, images} from '../assets';
import navigationService from './navigationService';

module.exports = {
    SW, SH, FULL_SW, FULL_SH, SCREEN_RATIO,
    videos, images,
    PropTypes,
    navigationService,
    connect,
    actions
};

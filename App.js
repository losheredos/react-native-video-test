import React from 'react';
import { Provider } from 'react-redux';
import store from './src/redux';
import Router from './src/router';
import {navigationService} from './src/utils';

const App = () => {
  return (
      <Provider store={store}>
          <Router ref={navigatorRef => {
              navigationService.setTopLevelNavigator(navigatorRef);
          }} />
      </Provider>
  );
};

export default App;
